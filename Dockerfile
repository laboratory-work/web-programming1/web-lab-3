FROM jboss/wildfly

RUN /opt/jboss/wildfly/bin/add-user.sh admin password --silent

USER root

RUN rm -rf /opt/jboss/wildfly/standalone/configuration/standalone_xml_history/current
RUN chmod -R 777 /opt/jboss/wildfly/standalone/configuration/
RUN chown -R jboss:jboss /opt/jboss/wildfly/

COPY ./server/wildfly/standalone.xml /opt/jboss/wildfly/standalone/configuration/standalone.xml

EXPOSE 18080 19990

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-P", "/standalone.properties"]