import Vue from 'vue';

const week = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

let clock = new Vue({
    data: {
        time: '',
        date: ''
    },
}).$mount('#clock');

let timerID = setInterval(updateTime, 1000);
updateTime();

function updateTime() {
    let cd = new Date();
    clock.time = zeroPadding(cd.getHours(), 2) + ':' +
        zeroPadding(cd.getMinutes(), 2) + ':' +
        zeroPadding(cd.getSeconds(), 2);
    clock.date = zeroPadding(cd.getFullYear(), 4) + '-' +
        zeroPadding(cd.getMonth()+1, 2) + '-' +
        zeroPadding(cd.getDate(), 2) + ' ' +
        week[cd.getDay()];
}

function zeroPadding(num, digit) {
    let zero = '';
    for(let i = 0; i < digit; i++) { zero += '0'; }
    return (zero + num).slice(-digit);
}