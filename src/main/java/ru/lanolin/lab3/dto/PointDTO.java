package ru.lanolin.lab3.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.lanolin.lab3.util.CheckAreaHit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;

@Data
@AllArgsConstructor
@EqualsAndHashCode(of = {"x", "y"})
public class PointDTO implements Serializable {
	private String x;
	private String y;
	private boolean[] r;

	public PointDTO() {
		this.x = "0";
		this.y = "0";
		this.r = new boolean[] { true, false, false, false, false };
	}

	public void setR(int i, boolean val){ this.r[i] = val; }

	public Boolean isHit(int i){
		return HIT.check(
				new BigDecimal(x),
				new BigDecimal(y),
				new BigDecimal(i)
		);
	}

	public static Boolean isHit(String x, String y, String r){
		return HIT.check(
				new BigDecimal(x),
				new BigDecimal(y),
				new BigDecimal(r)
		);
	}

	private static final CheckAreaHit HIT = (x, y, r) -> {

		if(x.compareTo(BigDecimal.valueOf(-6)) < 0 || x.compareTo(BigDecimal.valueOf(6)) > 0)
			return null;

		if(y.compareTo(BigDecimal.valueOf(-3)) < 0 || y.compareTo(BigDecimal.valueOf(5)) > 0)
			return null;

		int compareX = x.compareTo(BigDecimal.ZERO);
		int compareY = y.compareTo(BigDecimal.ZERO);

		if (compareX >= 0 && compareY >= 0) { //1 четверть
			return x.compareTo(r) <= 0 && y.compareTo(r.divide(BigDecimal.valueOf(2), MathContext.DECIMAL128)) <= 0;
		} else if (compareX <= 0 && compareY >= 0) { //2 четверть
			return y.compareTo(x.add(r)) <= 0;
		} else if (compareX <= 0) {  //3 четверть
			return false;
		} else {  //4 четверть
			return x.pow(2).add(y.pow(2)).compareTo(r.pow(2)) <= 0;
		}
	};
}
