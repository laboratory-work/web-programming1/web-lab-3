package ru.lanolin.lab3.util;

public interface ProfilingMXBean {
	void setEnable(boolean enable);
	boolean isEnable();
}
