package ru.lanolin.lab3.util;

import java.math.BigDecimal;

@FunctionalInterface
public interface CheckAreaHit {
	Boolean check(BigDecimal x, BigDecimal y, BigDecimal r);
}
