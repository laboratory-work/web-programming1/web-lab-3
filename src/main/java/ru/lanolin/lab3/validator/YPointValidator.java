package ru.lanolin.lab3.validator;

import lombok.extern.slf4j.Slf4j;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.math.BigDecimal;

@Slf4j
public class YPointValidator implements Validator {

	private static final Integer MIN = -3;
	private static final Integer MAX = 5;
//	private static final String fpRegex = "[\\x00-\\x20]*[+-]?(NaN|Infinity|((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)[pP][+-]?(\\p{Digit}+)))[fFdD]?)[\\x00-\\x20]*";

	@Override
	public void validate(FacesContext facesContext, UIComponent uiComponent, Object o) throws ValidatorException {
//		if (!Pattern.matches(fpRegex, yStr)) {
//			FacesMessage msg = new FacesMessage("Введено не число", "Доступныеу символы - 0..9");
//			throw new ValidatorException(msg);
//		}
		try {
			BigDecimal yB = (BigDecimal) o;

			int min = yB.compareTo(BigDecimal.valueOf(MIN));
			int max = yB.compareTo(BigDecimal.valueOf(MAX));

			if(min > 0 && max < 0)
				return;

			if(min < 1){
				FacesMessage msg = new FacesMessage("Выход за левую ганицу интервала", "Доступный интервал (-3..5)");
				throw new ValidatorException(msg);
			}else {
				FacesMessage msg = new FacesMessage("Выход за правю границу интервала", "Доступный интервал (-3..5)");
				throw new ValidatorException(msg);
			}

		}catch (NumberFormatException nfe){
			FacesMessage msg = new FacesMessage("Значение не является числом", "Доступныеу символы - 0..9");
			throw new ValidatorException(msg);
		}
	}
}
