package ru.lanolin.lab3.bean;

import lombok.Getter;
import ru.lanolin.lab3.util.ProfilingMXBean;

public class Profiling implements ProfilingMXBean {

	@Getter
	private boolean debugMode;

	public Profiling() {
		this.debugMode = false;
	}

	@Override
	public void setEnable(boolean enable) {
		this.debugMode = enable;
	}

	@Override
	public boolean isEnable() { return debugMode; }
}
