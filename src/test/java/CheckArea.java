import javafx.util.Pair;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.lanolin.lab3.dto.PointDTO;

import java.util.ArrayList;
import java.util.List;


public class CheckArea extends Assert {

	private List<Pair<Pair<String, String>, Pair<String, Boolean>>> pairs;

	@Before
	public void before(){
		pairs = new ArrayList<>();

		pairs.add(new Pair<>(new Pair<>("-2.727272727272727", "1.9090909090909092"), new Pair<>("1", false)));
		pairs.add(new Pair<>(new Pair<>("-2.727272727272727", "1.9090909090909092"), new Pair<>("2", false)));
		pairs.add(new Pair<>(new Pair<>("-2.727272727272727", "1.9090909090909092"), new Pair<>("3", false)));
		pairs.add(new Pair<>(new Pair<>("-2.727272727272727", "1.9090909090909092"), new Pair<>("4", false)));
		pairs.add(new Pair<>(new Pair<>("-2.727272727272727", "1.9090909090909092"), new Pair<>("5", true)));
		pairs.add(new Pair<>(new Pair<>("-3.090909090909091", "1.5909090909090908"), new Pair<>("5", true)));
		pairs.add(new Pair<>(new Pair<>("-3.590909090909091", "1.0909090909090908"), new Pair<>("5", true)));
		pairs.add(new Pair<>(new Pair<>("-2.909090909090909", "1.0909090909090908"), new Pair<>("5", true)));
		pairs.add(new Pair<>(new Pair<>("-2.6363636363636362", "1.0909090909090908"), new Pair<>("5", true)));

	}

	@Test
	public void test(){
		for (Pair<Pair<String, String>, Pair<String, Boolean>> pair : pairs) {
			String x = pair.getKey().getKey();
			String y = pair.getKey().getValue();
			Boolean check = PointDTO.isHit(x, y, pair.getValue().getKey());
			assertEquals(check, pair.getValue().getValue());
		}
	}

	@After
	public void after(){
		pairs.clear();
		pairs = null;
	}
}
